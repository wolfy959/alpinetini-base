# Alpinetini-base #
[![](https://images.microbadger.com/badges/image/wolfy959/alpinetini-base.svg)](https://microbadger.com/images/wolfy959/alpinetini-base "Get your own image badge on microbadger.com")

`alpinetini-base` is a base image for running [tini](https://github.com/krallin/tini) with [Alpine Linux](https://www.alpinelinux.org/) on [Docker](https://www.docker.com).


### Overview ###

* Version **1.0.3**
* Combines a tiny [Alpine Linux](https://www.alpinelinux.org/) os with a [tini](https://github.com/krallin/tini) init process
* Protects from *zombie processes* and ensures *default signal handlers* work<a name="why-tini">[1]</a>
* Download size of image = **2.5MB**
* On disk size of image ≅ **5.6MB**

### How to use ###

To use this as a base for another Docker image simply create a new Dockerfile and enter:
```
FROM wolfy959/alpinetini-base
```
Then add any commands or files you want to run. The **ENTRYPOINT** is already set to run [tini](https://github.com/krallin/tini) in this base image, so all that's needed is a **CMD** at the end of the file<a name="using-tini">[2]</a>:
```
CMD ["/your/program", "-and", "-its", "arguments"]
```
You can also directly run this image into the shell by running the following command:
```
docker run -it wolfy959/alpinetini-base /bin/sh
```
To check that tini is running as **PID 1** type `ps` into shell:
```
/ # ps
PID   USER     TIME   COMMAND
    1 root       0:00 /tini -- /bin/sh
    5 root       0:00 /bin/sh
    6 root       0:00 ps
```

---
- [1](#why-tini) - https://github.com/krallin/tini#why-tini
- [2](#using-tini) -  https://github.com/krallin/tini#using-tini
